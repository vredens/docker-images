TOPTARGETS := build test publish

SUBDIRS := $(wildcard ./*)

$(TOPTARGETS): $(SUBDIRS)
$(SUBDIRS):
	@[ ! -d $@ ] || $(MAKE) -C $@ $(MAKECMDGOALS)

.PHONY: $(TOPTARGETS) $(SUBDIRS)
