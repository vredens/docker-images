# Vredens' Docker Images

This is my personal collection of docker images from builders to running environments for certain applications.

Rule #1 is that these images should be very thin layers on top of official, or community winner, docker images.
Rule #2 add only the minimum common necessary and hide no magic behind BUILD layers
Rule #3 always KISS

# Images

* [docker-builder](docker-builder/README.md) adds some building tools on top of the docker official image, pretty cool stuff for running as your CI/CD bootstrap
* [golang](golang/README.md) a build and runtime environment for Golang projects
* [scratch](scratch/README.md) mostly for adding common static files to the `scratch` docker image
