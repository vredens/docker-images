# docker builder

Goal is to use this image as the builder container for CI/CD pipelines which run in dockerized environments, such as gitlab runner.

You should rely on building your own builder images with the necessary code and only use this image to bootstrap your dockerized building process.
