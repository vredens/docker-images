# Golang builder and runtime docker images

## Quick Start

### A sample .dockerignore

```
/vendor
/docker-compose*
*.md
/tmp
```

### A sample Dockerfile

```
FROM vredens/golang:alpine as builder

# allows this dockerfile to be somewhat generic
ARG PROJECT_ROOT
ARG PROJECT_NAME
ARG BIN_FOLDER

# install vendor
ADD Gopkg.toml Gopkg.lock /go/src/$PROJECT_ROOT/
RUN cd /go/src/$PROJECT_ROOT && dep ensure -vendor-only

# add source code
ADD . /go/src/$PROJECT_ROOT/

# set workdir
WORKDIR /go/src/$PROJECT_ROOT/

RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -a -installsuffix cgo -o /dist/$PROJECT_NAME $BIN_FOLDER

# package everything on a scratch
FROM vredens/scratch

# add your less modified assets here
COPY --from=builder /dist/ /bin/
# add your most modified resources here

WORKDIR /

# expose your dynamic resource folders in volumes for storage persistance
VOLUME /resources/assets/

EXPOSE 8080
```
