# vredens/scratch

Adds some necessary files for properly configuring `scratch` base image

Files added and reason

1. `/etc/ssl/certs/ca-certificates.crt`: needed for running Go statically compiled files
1. `/etc/nsswitch.conf`: needed for supporting defining hostname resolution set by docker runtime

## Quick Start

This image is what I currently use to package Golang binaries into usable containers.

Sample `Dockerfile`

```
FROM vredens/scratch

ADD myapp.cfg /etc/myapp/
ADD myapp /bin/

EXPOSE 9001
ENTRYPOINT ["/bin/myapp", "-c", "/etc/myapp/myapp.cfg"]
```
